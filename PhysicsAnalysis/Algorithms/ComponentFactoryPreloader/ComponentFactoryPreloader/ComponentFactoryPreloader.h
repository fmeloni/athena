/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COMPONENT_FACTORY_PRELOADER__COMPONENT_FACTORY_PRELOADER_H
#define COMPONENT_FACTORY_PRELOADER__COMPONENT_FACTORY_PRELOADER_H

namespace CP
{
  bool preloadComponentFactories ();
}

#endif
